<?php
namespace Vodaco\Sendloop;

class SystemDate extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function get(){
        $endpoint = 'system.systemdate.get';

        return parent::run($endpoint);
    }
}