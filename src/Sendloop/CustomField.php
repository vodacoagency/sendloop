<?php
namespace Vodaco\Sendloop;

class CustomField extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function getList($listId){
        $endpoint = 'customfield.getlist';

        $data = array(
            'ListID' => $listId,
        );

        return parent::run($endpoint, $data);
    }
}