<?php
namespace Vodaco\Sendloop;

class ReportList extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function overall($listId){
        $endpoint = 'report.list.overall';

        $data = array(
            'ListID' => $listId,
        );

        return parent::run($endpoint, $data);
    }
}