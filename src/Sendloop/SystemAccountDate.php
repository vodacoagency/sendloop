<?php
namespace Vodaco\Sendloop;

class SystemAccountDate extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function get(){
        $endpoint = 'system.accountdate.get';

        return parent::run($endpoint);
    }
}