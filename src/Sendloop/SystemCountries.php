<?php
namespace Vodaco\Sendloop;

class SystemCountries extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function get(){
        $endpoint = 'system.countries.get';

        return parent::run($endpoint);
    }
}