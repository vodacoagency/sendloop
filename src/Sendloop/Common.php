<?php
namespace Vodaco\Sendloop;

class Common extends Sendloop {
    protected $parent;
    protected $format;
    protected $apiKey;
    protected $commandOutput;

    public function __construct($parent) {
        $this->parent = $parent;
        $this->apiKey = $parent->getApiKey();
        $this->format = $parent->getFormat();
        $this->commandOutput = $parent->getCommandOutput();
    }

    public function run($endpoint, $parameters = array()){
        return $this->parent->run($endpoint, $parameters);
    }
}