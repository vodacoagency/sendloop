<?php
namespace Vodaco\Sendloop;

class DataCampaign extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function unsubscriptions($campaignId, $settings){
        $endpoint = 'data.campaign.unsubscriptions';

        $data = array(
            'CampaignID' => $campaignId,
        );
        $data = array_merge($data, $settings);

        return parent::run($endpoint, $data);
    }

    public function openLocations($campaignId, $settings){
        $endpoint = 'data.campaign.openlocations';

        $data = array(
            'CampaignID' => $campaignId,
        );
        $data = array_merge($data, $settings);

        return parent::run($endpoint, $data);
    }

    public function forwards($campaignId, $settings){
        $endpoint = 'data.campaign.forwards';

        $data = array(
            'CampaignID' => $campaignId,
        );
        $data = array_merge($data, $settings);

        return parent::run($endpoint, $data);
    }

    public function linkClicks($campaignId, $settings){
        $endpoint = 'data.campaign.linkclicks';

        $data = array(
            'CampaignID' => $campaignId,
        );
        $data = array_merge($data, $settings);

        return parent::run($endpoint, $data);
    }

    public function browserViews($campaignId, $settings){
        $endpoint = 'data.campaign.browserviews';

        $data = array(
            'CampaignID' => $campaignId,
        );
        $data = array_merge($data, $settings);

        return parent::run($endpoint, $data);
    }

    public function bounces($campaignId, $settings){
        $endpoint = 'data.campaign.bounces';

        $data = array(
            'CampaignID' => $campaignId,
        );
        $data = array_merge($data, $settings);

        return parent::run($endpoint, $data);
    }
}