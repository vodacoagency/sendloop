<?php
namespace Vodaco\Sendloop;

class SubscriberList extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function create($name, $optInMode='Double') {
        $endpoint = 'list.create';

        $data = array(
            'Name' => $name,
            'OptInMode' => $optInMode
        );
        
        return parent::run($endpoint, $data);
    }

    public function update($listId, $name, $optInMode='Double') {
        $endpoint = 'list.update';

        $data = array(
            'ListID' => $listId,
            'Name' => $name,
            'OptInMode' => $optInMode
        );
        
        return parent::run($endpoint, $data);
    }

    public function get($listId, $customFields=0) {
        $endpoint = 'list.get';

        $data = array(
            'ListID' => $listId,
            'GetCustomFields' => $customFields
        );
        
        return parent::run($endpoint, $data);
    }

    public function getList() {
        $endpoint = 'list.getlist';
        
        return parent::run($endpoint);
    }

    public function settings() {
        return new SubscriberListSettings($this);
    }
}