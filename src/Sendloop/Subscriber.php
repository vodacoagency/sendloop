<?php
namespace Vodaco\Sendloop;

class Subscriber extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function browse($listId, $segmentId=0, $startIndex=0) {
        $endpoint = 'subscriber.browse';

        $data = array(
            'ListID' => $listId,
            'SegmentID' => $segmentId,
            'StartIndex' => $startIndex
        );
        
        return parent::run($endpoint, $data);
    }

    public function get($listId, $data) {
        $endpoint = 'subscriber.get';

        $data['ListID'] =  $listId;
        
        return parent::run($endpoint, $data);
    }

    public function import($listId, $data) {
        $endpoint = 'Subscriber.Import';

        $data['ListID'] =  $listId;
        
        return parent::run($endpoint, $data);
    }

    public function search($email) {
        $endpoint = 'subscriber.search';

        $data['EmailAddress'] =  $email;
        
        return parent::run($endpoint, $data);
    }

    public function subscribe($listId, $email, $fields=null) {
        $endpoint = 'subscriber.subscribe';

        $data = array(
            'ListID' =>  $listId,
            'EmailAddress' =>  $email,
            'SubscriptionIP' =>  $this->getIP()
        );

        if(is_array($fields)) {
            foreach($fields as $key => $value) {
                $data['Fields'][$key] = $value;
            }
        }

        return parent::run($endpoint, $data);
    }

    public function unsubscribe($listId, $email) {
        $endpoint = 'subscriber.unsubscribe';

        $data = array(
            'ListID' =>  $listId,
            'EmailAddress' =>  $email,
            'UnsubscriptionIP' =>  $this->getIP(),
        );
        
        return parent::run($endpoint, $data);
    }

    public function update($listId, $subscriberId, $email, $fields=null) {
        $endpoint = 'subscriber.update';

        $customFields = parent::customField()->getList($listId);
        $customFieldNames = [];
        foreach($customFields->CustomFields as $cf) {
            $customFieldNames[$cf->CustomFieldID] = $cf->FieldName;
        }
        
        $data = array(
            'ListID' =>  $listId,
            'SubscriberID' =>  $subscriberId,
            'EmailAddress' =>  $email
        );

        if(is_array($fields)) {
            foreach($fields as $key => $value) {
                $data['Fields']['CustomField'.array_search($key, $customFieldNames)] = $value;
            }
        }
        
        return parent::run($endpoint, $data);
    }
}