<?php
namespace Vodaco\Sendloop;

class SystemTimezones extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function get(){
        $endpoint = 'system.timezones.Get';

        return parent::run($endpoint);
    }
}