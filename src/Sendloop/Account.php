<?php
namespace Vodaco\Sendloop;

class Account extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }
    
    public function get() {
        $endpoint = 'account.info.get';
        
        return parent::run($endpoint);
    }

    public function update(array $data) {
        $endpoint = 'account.info.update';

        return parent::run($endpoint, $data);
    }
}