<?php
namespace Vodaco\Sendloop;

class Sendloop {
    private $host = 'http://app.sendloop.com/api/v3';
    protected $format = 'json';
    protected $apiKey;
    protected $commandOutput;

    public function __construct($apiKey='') {
        $this->apiKey = $apiKey;
    }
    
    private function makeUrl($endpoint) {
        $format = in_array($this->format, array('json_array', 'json_object')) ? 'json' : $this->format;
        return $this->host.DIRECTORY_SEPARATOR.$endpoint.DIRECTORY_SEPARATOR.$format;
    }

    protected function run($endpoint, $parameters = array()) {
        $endpoint = $this->makeUrl($endpoint);
        $ch = curl_init($endpoint);

        if($this->commandOutput) {
            $this->setCommandOutput(false);
            return $this->generateCurlCommand($endpoint, $parameters, array(
                'apikey: '.$this->apiKey,
                'content-type: application/x-www-form-urlencoded'
            ));
        }
        
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'apikey: '.$this->apiKey,
            'content-type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($parameters));
        
        $output = curl_exec($ch);
        
        curl_close($ch);

        switch($this->format) {
            case 'php':
                $output = unserialize($output);
                break;

            case 'json_object':
                $output = json_decode($output);
                break;

            case 'json_array':
                $output = json_decode($output, true);
                break;

            case 'json':
                header('Content-Type: application/json');
                break;
        }

        return $output;
    }

    protected function getIP() {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
    
        if(filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
    
        return $ip;
    }

    protected function generateCurlCommand($url, array $data=null, array $headers=null) {
        $cmd = 'curl';
        
        $params[] = '--request POST';
        $params[] = '--url '.$url;

        if ($headers !== null) {
            foreach($headers as $key => $value) {
                $params[] = "--header '$value'"; 
            }
        }
        
        if ($data !== null) {
            $params[] = "--data '" . rawurldecode(http_build_query($data))."'"; 
        }

        return $cmd . ' ' . implode(' ', $params);
    }
    
    /**
     * Get the value of apiKey
     */ 
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set the value of apiKey
     *
     * @return  self
     */ 
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }    

	/**
	 * getCommandOutput.
	 *
	 * @return	mixed
	 */
	public function getCommandOutput() {
		return $this->commandOutput;
	}

	/**
	 * setCommandOutput.
	 *
	 * @param	mixed	$commandOutput	
	 * @return	self
	 */
	public function setCommandOutput($commandOutput) {
		$this->commandOutput = $commandOutput;

		return $this;
	}

    /**
     * Get the value of host
     */ 
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set the value of host
     *
     * @return  self
     */ 
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get the value of format
     */ 
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set the value of format
     *
     * @return  self
     */ 
    public function setFormat($format)
    {
        if(!in_array($format, array('json', 'json_object', 'json_array', 'php', 'xml'))) {
            throw new \Exception("Invalid output format.");
        }

        $this->format = $format;

        return $this;
    }

    public function account() 
    {
        return new Account($this);
    }

    public function list() 
    {
        return new SubscriberList($this);
    }

    public function subscriber() 
    {
        return new Subscriber($this);
    }

    public function suppressionList() 
    {
        return new SuppressionList($this);
    }

    public function campaign() 
    {
        return new Campaign($this);
    }

    public function export() 
    {
        return new Export($this);
    }

    public function report() 
    {
        return new Report($this);
    }

    public function data() 
    {
        return new Data($this);
    }

    public function system() 
    {
        return new System($this);
    }

    public function customField() 
    {
        return new CustomField($this);
    }
}