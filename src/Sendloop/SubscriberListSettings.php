<?php
namespace Vodaco\Sendloop;

class SubscriberListSettings extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function get($listId) {
        $endpoint = 'list.settings.get';

        $data = array(
            'ListID' => $listId
        );
        
        return parent::run($endpoint, $data);
    }
        
    public function update($listId, array $data) {
        $endpoint = 'list.settings.update';

        $data['ListID'] = $listId;

        return parent::run($endpoint, $data);
    }    
}