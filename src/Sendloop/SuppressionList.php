<?php
namespace Vodaco\Sendloop;

class SuppressionList extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function getList($listId){
        $endpoint = 'suppression.list.getlist';

        $data = array(
            'ListID' => $listId
        );

        return parent::run($endpoint, $data);
    }

    public function get($email){
        $endpoint = 'suppression.list.get';

        $data = array(
            'EmailAddress' => $email
        );

        return parent::run($endpoint, $data);
    }

    public function add($listId, $email){
        $endpoint = 'suppression.list.add';

        $data = array(
            'ListID' => $listId,
            'EmailAddress' => $email
        );

        return parent::run($endpoint, $data);
    }
}