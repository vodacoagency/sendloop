<?php
namespace Vodaco\Sendloop;

class Data extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function campaign(){
        return new ReportCampaign($this);
    }
}