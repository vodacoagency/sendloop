<?php
namespace Vodaco\Sendloop;

class Report extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function list(){
        return new ReportList($this);
    }

    public function campaign(){
        return new ReportCampaign($this);
    }
}