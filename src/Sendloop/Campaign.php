<?php
namespace Vodaco\Sendloop;

class Campaign extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function create($data){
        $endpoint = 'campaign.create';

        $tempData = array(
            'Version' => 2,
            'Name' => '',
            'FromName' => '',
            'FromEmail' => '',
            'ReplyToName' => '',
            'ReplyToEmail' => '',
            'Recipients' => [],
            'Subject' => '',
            'PlainContent' => '',
            'HTMLContent' => ''
        );

        $data = array_merge($tempData, $data);

        return parent::run($endpoint, $data);
    }

    public function update($campaignId, $data=array()){
        $endpoint = 'campaign.update';

        $tempData = array(
            'CampaignID' => $campaignId
        );

        $data = array_merge($tempData, $data);

        return parent::run($endpoint, $data);
    }

    public function send($campaignId, $sendDate='NOW'){
        $endpoint = 'campaign.send';

        $data = array(
            'CampaignID' => $campaignId,
            'SendDate' => $sendDate,
        );

        return parent::run($endpoint, $data);
    }

    public function pause($campaignId){
        $endpoint = 'campaign.pause';

        $data = array(
            'CampaignID' => $campaignId
        );

        return parent::run($endpoint, $data);
    }

    public function resume($campaignId){
        $endpoint = 'campaign.resume';

        $data = array(
            'CampaignID' => $campaignId
        );

        return parent::run($endpoint, $data);
    }

    public function cancelSchedule($campaignId){
        $endpoint = 'campaign.cancel';

        $data = array(
            'CampaignID' => $campaignId
        );

        return parent::run($endpoint, $data);
    }

    public function getListByStatus($status, $settings){
        $endpoint = 'campaign.getlistbystatus';

        $data = array(
            'CampaignStatus' => $status
        );

        $data = array_merge($data, $settings);
        //dd($data);
        return parent::run($endpoint, $data);
    }

    public function getList($sent=true, $drafts=false, $sending=false, $paused=false, $failed=false, $approval=false) {
        $endpoint = 'campaign.getlist';

        $data = array(
            'IgnoreDraft' => !$drafts,
            'IgnoreSending' => !$sending,
            'IgnorePaused' => !$paused,
            'IgnoreSent' => !$sent,
            'IgnoreFailed' => !$failed,
            'IgnoreApproval' => !$approval
        );

        return parent::run($endpoint, $data);
    }

    public function get($campaignId){
        $endpoint = 'campaign.get';

        $data = array(
            'CampaignID' => $campaignId
        );

        return parent::run($endpoint, $data);
    }

    public function delete($campaignId){
        $endpoint = 'campaign.delete';

        $data = array(
            'CampaignID' => $campaignId
        );

        return parent::run($endpoint, $data);
    }
}