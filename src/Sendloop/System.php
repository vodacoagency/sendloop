<?php
namespace Vodaco\Sendloop;

class System extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function timezones(){
        return new SystemTimezones($this);
    }

    public function systemDate(){
        return new SystemDate($this);
    }

    public function countries(){
        return new SystemCountries($this);
    }

    public function accountDate(){
        return new SystemAccountDate($this);
    }
}