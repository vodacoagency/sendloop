<?php
namespace Vodaco\Sendloop;

class ReportCampaign extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function webBrowserViews($campaignId){
        $endpoint = 'report.campaign.webbrowserviews';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function unsubscriptions($campaignId){
        $endpoint = 'report.campaign.unsubscriptions';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function spamComplaints($campaignId){
        $endpoint = 'report.campaign.spamcomplaints';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function overall($campaignId){
        $endpoint = 'report.campaign.overall';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function opens($campaignId){
        $endpoint = 'report.campaign.opens';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function openLocations($campaignId){
        $endpoint = 'report.campaign.openlocations';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function links($campaignId){
        $endpoint = 'report.campaign.links';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function forwards($campaignId){
        $endpoint = 'report.campaign.forwards';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function emailClients($campaignId){
        $endpoint = 'report.campaign.emailclients';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }

    public function bounces($campaignId){
        $endpoint = 'report.campaign.bounces';

        $data = array(
            'CampaignID' => $campaignId,
        );

        return parent::run($endpoint, $data);
    }
}