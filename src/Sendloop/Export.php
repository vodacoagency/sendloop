<?php
namespace Vodaco\Sendloop;

class Export extends Common {
    public function __construct($parent) {
        parent::__construct($parent);
    }

    public function unsubscriptions($listId){
        $endpoint = 'export.unsubscriptions';

        $data = array(
            'ListID' => $listId,
        );

        return parent::run($endpoint, $data);
    }

    public function suppressionList(){
        $endpoint = 'export.suppressionlist';

        return parent::run($endpoint);
    }

    public function spamComplaints(){
        $endpoint = 'export.spamcomplaints';

        return parent::run($endpoint);
    }

    public function bounces($bounceType){
        $endpoint = 'export.bounces';

        $data = array(
            'BounceType' => $bounceType
        );

        return parent::run($endpoint, $data);
    }
}